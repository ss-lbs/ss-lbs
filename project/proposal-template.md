# CPS 474/574 Software/Language-based Security

### Fall 2022

### Dr. Phu Phung

# Team Project: [Include Your Team Project Title] 

### Team workspace: [Include your team workspace URL]

## Team members

* Member 1, email
* Member 2, email
    

## Summary of Background
(Expected length: 1 paragraph)

## Project objectives
(Expected length: Some bullet points)

## Expected contributions and relevance to the course topics
(Expected length: two sentences)

## Your team plan: 

How your team members contribute to the project, the work plan. 
(Expected length:  1 paragraph)

## Timeline: 

Particular intermediate tasks and timeframe to complete the task. The timeline is weekly and should start from September 21 to the final report deadline.

_NOTE: Your project should contain experiment results in order to get high points. Your proposal should be one page in PDF format. Refer this guidance for writing in Markdown and how to convert a Markdown to PDF: https://www.markdownguide.org/getting-started/_

