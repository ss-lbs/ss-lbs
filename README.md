# Repository for CPS474/CPS574 Software Security/Language-based Security
## Instructor: Dr. Phu Phung
## Department of Computer Science
## University of Dayton

### To clone:

Open a terminal in your Linux shell, and type:

```shell
git clone https://bitbucket.org/ss-lbs/ss-lbs.git 
```